package uwu.serenya.silly_road.registry;

import io.wispforest.owo.registration.reflect.ItemRegistryContainer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import uwu.serenya.silly_road.SillyRoads;
import uwu.serenya.silly_road.items.paint.PaintBrushItem;
import uwu.serenya.silly_road.items.paint.PaintScraperItem;

import java.util.function.Function;
import java.util.function.Supplier;

public class SillyRoadItems {

    public static final PaintBrushItem PAINT_BRUSH = item("paint_brush", PaintBrushItem::new,
            new FabricItemSettings().stacksTo(1));

    public static final PaintScraperItem PAINT_SCRAPER = item("paint_scraper", PaintScraperItem::new,
            new FabricItemSettings().stacksTo(1));
    public static final Item BITUMEN = item("bitumen", Item::new);
    public  static final Item ASPHALT = SillyRoadBlocks.ASPHALT.asItem();
    public  static final Item CONCRETE = SillyRoadBlocks.CONCRETE.asItem();

    private static <I extends Item> I item(String id, Function<Item.Properties, I> factory, Item.Properties props) {
        return Registry.register(BuiltInRegistries.ITEM, SillyRoads.asLocation(id), factory.apply(props));
    }

    private static <I extends Item> I item(String id, Function<Item.Properties, I> factory) {
        return item(id, factory, new FabricItemSettings());
    }

    public static void register() {}

}
