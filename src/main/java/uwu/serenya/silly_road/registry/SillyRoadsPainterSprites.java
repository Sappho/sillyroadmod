package uwu.serenya.silly_road.registry;

import company.sappho.painter.api.PainterSprite;
import uwu.serenya.silly_road.SillyRoads;

import java.util.HashSet;
import java.util.Set;

import static uwu.serenya.silly_road.SillyRoads.asLocation;
public class SillyRoadsPainterSprites {


    public static final Set<PainterSprite> ROAD_LINES = new HashSet<>();
    public static final PainterSprite DEBUG = PainterSprite.createSimple(
            asLocation("debug"), asLocation("block/road/debug")
    ).register();

    public static void register() {
        for(int i = 1; i <= 323; i++) {
            ROAD_LINES.add(PainterSprite.createSimple(
                    asLocation("road/%d".formatted(i)),
                    asLocation("block/road/%d".formatted(i))
            ).register());
        }
    }
}
