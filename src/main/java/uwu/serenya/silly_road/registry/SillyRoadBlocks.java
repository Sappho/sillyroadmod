package uwu.serenya.silly_road.registry;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import uwu.serenya.silly_road.SillyRoads;
import uwu.serenya.silly_road.blocks.pole.PoleBlock;

import java.util.function.Function;

import static net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings.copyOf;

public class SillyRoadBlocks {

    public static final Block ASPHALT = stoneBlock("asphalt", Block::new);
    public static final Block CONCRETE = stoneBlock("concrete", Block::new);
    public static final Block POLE = block("pole", PoleBlock::new, copyOf(Blocks.STONE).sound(SoundType.NETHERITE_BLOCK));

    private static <B extends Block> B block(String id, Function<BlockBehaviour.Properties, B> factory, BlockBehaviour.Properties props) {
        B block = Registry.register(BuiltInRegistries.BLOCK, SillyRoads.asLocation(id), factory.apply(props));
        Registry.register(BuiltInRegistries.ITEM, SillyRoads.asLocation(id), new BlockItem(block, new FabricItemSettings()));
        return block;
    }

    private static <B extends Block> B stoneBlock(String id, Function<BlockBehaviour.Properties, B> factory) {
        return block(id, factory, FabricBlockSettings.copyOf(Blocks.STONE));
    }
    public static void register() {}


}
