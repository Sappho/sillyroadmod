package uwu.serenya.silly_road.registry;

import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.fabricmc.fabric.impl.biome.modification.BuiltInRegistryKeys;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import uwu.serenya.silly_road.SillyRoads;

import java.awt.*;

public class SillyRoadCreativeTab {

    public static final ResourceKey<CreativeModeTab> MAIN = ResourceKey.create(Registries.CREATIVE_MODE_TAB, SillyRoads.asLocation("main"));

    public static void register() {
        Registry.register(BuiltInRegistries.CREATIVE_MODE_TAB, MAIN, FabricItemGroup.builder()
                .icon(() -> new ItemStack(SillyRoadItems.BITUMEN))
                .title(Component.translatable("itemGroup.sillyroads.main"))
                .build()
        );

        ItemGroupEvents.modifyEntriesEvent(MAIN).register(group -> {
            group.accept(new ItemStack(SillyRoadItems.PAINT_BRUSH));
            group.accept(new ItemStack(SillyRoadItems.PAINT_SCRAPER));
            group.accept(new ItemStack(SillyRoadItems.BITUMEN));
            group.accept(new ItemStack(SillyRoadBlocks.ASPHALT.asItem()));
            group.accept(new ItemStack(SillyRoadBlocks.CONCRETE.asItem()));

        });


    }
}
