package uwu.serenya.silly_road.util;

import net.minecraft.core.Direction;
import org.joml.Vector2f;

public class Util {
    public static float snapRotation(float rot) {
        if(rot > -45.0 && rot < 45.0)
            return 180f;
        if(rot > 45.0 && rot < 135.0)
            return 90f;
        if(rot < -135.0 || rot > 135.0)
            return 0f;
        if(rot > -135.0 && rot < -45.0)
            return -90f;
        throw new IllegalArgumentException("invalid rotation");
    }

    public static int[][] getUVDirection(Direction dir) {
        return switch (dir){
            case DOWN -> null; case UP -> null; // These Directions can't exist

            //TODO: Actually implement the math instead of just mapping the uv using static values
            case SOUTH -> new int[][]{new int[]{1, 1}, new int[]{1, 0}, new int[]{0, 0}, new int[]{0,1}};
            case EAST ->  new int[][]{new int[] {1, 0}, new int[]{0, 0}, new int[]{0, 1}, new int[]{1, 1}};
            case NORTH -> new int[][]{new int[] {0, 0}, new int[]{0, 1}, new int[]{1, 1}, new int[]{1, 0}};
            case WEST -> new int[][]{new int[] {0, 1}, new int[]{1, 1}, new int[]{1, 0}, new int[]{0,0}};
        };
    }
}
