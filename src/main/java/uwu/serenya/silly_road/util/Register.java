package uwu.serenya.silly_road.util;

import net.minecraft.world.level.saveddata.SavedData;

public interface Register<T> {
    default void register() {

    }
    T[] getValues();
}
