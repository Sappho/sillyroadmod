package uwu.serenya.silly_road.util;

import net.minecraft.util.StringRepresentable;
import net.minecraft.world.item.DyeColor;

import java.util.Optional;

public enum RoadColor implements StringRepresentable {
    WHITE(0, "white", 0xFFF9FFFE),
    ORANGE(1, "orange", 0xFFDD5100),
    MAGENTA(2, "magenta", 0xffff0fde),
    LIGHT_BLUE(3, "light_blue", 0xFF00EAFF),
    YELLOW(4, "yellow", 0xFFF8D304),
    LIME(5, "lime", 0xff1bff0f),
    PINK(6, "pink", 0xffff94f0),
    GRAY(7, "gray", 0xff696969),
    LIGHT_GRAY(8, "light_gray", 0xffb8b8b8),
    CYAN(9, "cyan", 0xff00afb8),
    PURPLE(10, "purple", 0xff8100ed),
    BLUE(11, "blue", 0xff0003a3),
    BROWN(12, "brown", 0xff572f00),
    GREEN(13, "green", 0xff006b07),
    RED(14, "red", 0xffff0d05),
    BLACK(15, "black", 0xff191919);

    public final int index;
    public final String name;
    public final int color;

    RoadColor(int index, String name, int color) {
        this.index = index;
        this.name = name;
        this.color = color;
    }

    @Override
    public String getSerializedName() {
        return name;
    }

    public static RoadColor of(DyeColor color) {
        return Enum.valueOf(RoadColor.class, color.getName().toUpperCase());
    }

    public static Optional<RoadColor> fromColor(int color) {
        for(RoadColor value : RoadColor.values()) {
            if(value.color == color) return Optional.of(value);
        }
        return Optional.empty();
    }
}
