package uwu.serenya.silly_road;

import company.sappho.painter.PainterAPI;
import company.sappho.painter.api.BlockFace;
import company.sappho.painter.api.PainterSprite;
import company.sappho.painter.event.PainterEvents;
import io.wispforest.owo.registration.reflect.FieldRegistrationHandler;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.player.PlayerBlockBreakEvents;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import uwu.serenya.silly_road.network.SillyRoadsNetwork;
import uwu.serenya.silly_road.registry.SillyRoadBlocks;
import uwu.serenya.silly_road.registry.SillyRoadCreativeTab;
import uwu.serenya.silly_road.registry.SillyRoadItems;
import uwu.serenya.silly_road.registry.SillyRoadsPainterSprites;

import static uwu.serenya.silly_road.Constants.MOD_ID;

public class SillyRoads implements ModInitializer {

	@Override
	public void onInitialize() {
		Constants.LOGGER.info("Initializing silly roads");

		SillyRoadBlocks.register();
		SillyRoadItems.register();
		SillyRoadsNetwork.register();
		SillyRoadCreativeTab.register();
		SillyRoadsPainterSprites.register();


		PlayerBlockBreakEvents.BEFORE.register((level, player, pos, state, blockEntity) -> {
            if(player.getMainHandItem().is(SillyRoadItems.PAINT_BRUSH) && player.isCreative()) {
				if(PainterAPI.INSTANCE.remove(level, new BlockFace(pos, Direction.UP))) {
					level.playLocalSound(pos, SoundEvents.AXE_SCRAPE, SoundSource.BLOCKS, 0.5f, 2.0f, false);
					return false;
				}
			}
			return true;
        });


		// Datafixer from old version of PainterAPI
		PainterEvents.MODIFY_LOAD.register((key, container) -> {
			if(!key.getNamespace().equals(MOD_ID)) return;
			String oldPathPre = "textures/block/road/";
			String oldPathSuffix = ".png";
			String path = key.getPath();

			if(path.startsWith(oldPathPre) && path.endsWith(oldPathSuffix)) {
				String id = path.substring(oldPathPre.length(), path.length() - oldPathSuffix.length());
				container.set(PainterSprite.get(asLocation("road/" + id)));
			}
		});

	}

    public static ResourceLocation asLocation(String id) {
        return new ResourceLocation(MOD_ID, id);
    }
}
