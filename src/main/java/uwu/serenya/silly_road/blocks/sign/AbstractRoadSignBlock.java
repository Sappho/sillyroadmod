package uwu.serenya.silly_road.blocks.sign;

import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import org.jetbrains.annotations.Nullable;
import uwu.serenya.silly_road.registry.SillyRoadBlocks;

public class AbstractRoadSignBlock extends Block {

    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
    public static final BooleanProperty POLE = BooleanProperty.create("pole");

    public AbstractRoadSignBlock(Properties properties) {
        super(properties);
        this.defaultBlockState()
                .setValue(FACING, Direction.NORTH)
                .setValue(POLE, false);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(FACING, POLE);
    }

    @Override
    public boolean hasDynamicShape() {
        return true;
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        BlockState clicked = context.getLevel().getBlockState(context.getClickedPos());
        BlockState belowPlaced =
                context.getLevel().getBlockState(context.getClickedPos().relative(context.getClickedFace()).below());

        Direction placementDir = (context.getPlayer() != null) ? context.getPlayer().getDirection() : Direction.NORTH;

        if(clicked.is(SillyRoadBlocks.POLE) && belowPlaced.is(SillyRoadBlocks.POLE))
            return defaultBlockState().setValue(POLE, true).setValue(FACING, placementDir);

        return defaultBlockState().setValue(FACING, context.getClickedFace().getOpposite());
    }
}
