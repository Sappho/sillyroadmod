package uwu.serenya.silly_road.blocks.pole;

import net.minecraft.BlockUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import org.jetbrains.annotations.Nullable;
import uwu.serenya.silly_road.registry.SillyRoadBlocks;

public class PoleBlock extends Block {

    public static BooleanProperty BOTTOM = BooleanProperty.create("bottom");
    public PoleBlock(Properties properties) {
        super(properties);
        this.defaultBlockState().setValue(BOTTOM, true);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(BOTTOM);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        BlockState blockBelow = context.getLevel().getBlockState(context.getClickedPos().below());

        return blockBelow.is(SillyRoadBlocks.POLE)
                ? defaultBlockState().setValue(BOTTOM, false) : defaultBlockState();
    }
}
