package uwu.serenya.silly_road.client.screens;

import com.mojang.blaze3d.systems.RenderSystem;
import io.wispforest.owo.ui.base.BaseOwoScreen;
import io.wispforest.owo.ui.component.ButtonComponent;
import io.wispforest.owo.ui.component.Components;
import io.wispforest.owo.ui.container.Containers;
import io.wispforest.owo.ui.container.FlowLayout;
import io.wispforest.owo.ui.container.GridLayout;
import io.wispforest.owo.ui.core.*;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.network.chat.Component;
import org.jetbrains.annotations.NotNull;
import uwu.serenya.silly_road.Constants;
import uwu.serenya.silly_road.SillyRoads;
import uwu.serenya.silly_road.network.BrushPacket;
import uwu.serenya.silly_road.network.SillyRoadsNetwork;

@Environment(EnvType.CLIENT)
public class PaintBrushScreen extends BaseOwoScreen<FlowLayout> {

    int variant = 1;

    public PaintBrushScreen(int variant) {
        this.variant = variant;
    }
    @Override
    protected @NotNull OwoUIAdapter<FlowLayout> createAdapter() {
        return OwoUIAdapter.create(this, Containers::verticalFlow);
    }

    @Override
    protected void build(FlowLayout rootComponent) {

        rootComponent
                .surface(Surface.VANILLA_TRANSLUCENT)
                .horizontalAlignment(HorizontalAlignment.CENTER)
                .verticalAlignment(VerticalAlignment.CENTER);

        FlowLayout container = (FlowLayout) Containers.verticalFlow(Sizing.content(), Sizing.content())
                .child(Components.label(Component.nullToEmpty("Paint Brush")).margins(Insets.of(0, 4, 0, 0)))
                .padding(Insets.both(5, 5))
                .surface(Surface.DARK_PANEL)
                .verticalAlignment(VerticalAlignment.CENTER)
                .horizontalAlignment(HorizontalAlignment.CENTER);

        GridLayout roadMarkings = Containers.grid(Sizing.content(), Sizing.content(), 100, 4);

        int column = 0;
        int row = 0;
        for(int i = 1; i <= Constants.LINE_COUNT; i++) {
            int uwu = i;
            roadMarkings.child(
                    Containers.grid(Sizing.content(), Sizing.content(), 1,1)
                            .child(Components.button(Component.nullToEmpty(""), bc -> variant = uwu)
                                    .renderer((context, button, delta) -> {
                                        RenderSystem.enableDepthTest();

                                        context.drawPanel(button.getX(), button.getY(), button.width(), button.height(),
                                                !drawButtonActive(button, uwu));
                                        context.blit(SillyRoads.asLocation("textures/block/road/" + uwu + ".png"),
                                                button.getX() + button.width() / 4,
                                                button.getY() + button.height() / 4,
                                                0, 0,
                                                button.getWidth() / 2, button.getHeight() / 2 ,
                                                button.getWidth() / 2, button.getHeight() / 2);

                            }).sizing(Sizing.fixed(24)), 0, 0)
                            .margins(Insets.of(1, 1, 1, 1)), row, column
            );
            column++;
            if(column == 4) {
                column = 0;
                row++;
            }
        }

        container.child(Containers.verticalScroll(Sizing.content(2), Sizing.fixed(128), roadMarkings)
                //.scrollTo(roadMarkings.children().get(variant - 1))
        );
        rootComponent.child(container);
    }

    private boolean drawButtonActive(ButtonComponent button, int i) {
        return button.isHoveredOrFocused() || i == variant;
    }

    @Override
    public void onClose() {
        super.onClose();
        SillyRoadsNetwork.BRUSH.clientHandle().send(new BrushPacket(variant));
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }
}
