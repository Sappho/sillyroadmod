package uwu.serenya.silly_road.client.screens;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.Minecraft;

@Environment(EnvType.CLIENT)
public class PaintBrushClient {

    public static void openPaintBrushScreen(int variant) {
        PaintBrushScreen screen = new PaintBrushScreen(variant);
        Minecraft.getInstance().setScreen(screen);
    }
}
