package uwu.serenya.silly_road.items.paint;

import company.sappho.painter.PainterAPI;
import company.sappho.painter.api.BlockFace;
import company.sappho.painter.api.PainterSprite;
import io.wispforest.owo.network.ServerAccess;
import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.DyeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.NotNull;
import uwu.serenya.silly_road.SillyRoads;
import uwu.serenya.silly_road.client.screens.PaintBrushClient;
import uwu.serenya.silly_road.network.BrushPacket;
import uwu.serenya.silly_road.util.RoadColor;
import uwu.serenya.silly_road.util.Util;

public class PaintBrushItem extends Item {
    public PaintBrushItem(Properties properties) {
        super(properties);
    }

    @Override
    public @NotNull InteractionResult useOn(UseOnContext context) {

        BlockPos pos = context.getClickedPos();
        Level world = context.getLevel();
        CompoundTag tag = context.getItemInHand().getTag();
        boolean result;


        if(tag == null || tag.getInt("variant") == 0) {
            context.getPlayer().displayClientMessage(Component.literal("No pattern selected")
                    .withStyle(ChatFormatting.RED), true);
            return InteractionResult.FAIL;
        }

        ItemStack dye = context.getPlayer().getOffhandItem();
        DyeColor baseColor = (dye.getItem() instanceof DyeItem d) ? d.getDyeColor() : DyeColor.WHITE;
        float rotation = Util.snapRotation(
                (context.getClickedFace().getAxis() == Direction.Axis.Y)
                        ? context.getPlayer().getYRot()
                        : context.getRotation()
        );


        BlockFace loc = new BlockFace(context.getClickedPos(), context.getClickedFace());

        if(PainterAPI.INSTANCE.isPainted(context.getLevel(), loc)) PainterAPI.INSTANCE.remove(context.getLevel(), loc);

        world.playLocalSound(pos, SoundEvents.BRUSH_GENERIC, SoundSource.BLOCKS, 0.7f, 1.0f, true);
        result = PainterAPI.INSTANCE.paint(
                PainterSprite.get(SillyRoads.asLocation("road/%d".formatted(tag.getInt("variant")))),
                context.getLevel(),
                new BlockFace(context.getClickedPos(), context.getClickedFace()),
                RoadColor.of(baseColor).color,
                rotation
        );


        //SillyRoads.asLocation("textures/block/road/%d.png".formatted(tag.getInt("variant"))),
        //}

        return result ? InteractionResult.SUCCESS : InteractionResult.PASS;

    }

    @Override
   public @NotNull InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand usedHand) {

        ItemStack stack = player.getItemInHand(usedHand);
        CompoundTag tag = stack.getTag();

        if(tag == null) {
            tag = new CompoundTag();
            stack.setTag(tag);
        }
        if(level.isClientSide) PaintBrushClient.openPaintBrushScreen(tag.getInt("variant"));

        return super.use(level, player, usedHand);
    }


//    public boolean sillyroads$removePaint(ItemStack itemstack, BlockPos pos, Player player) {
//        if(!player.isCreative()) return false;
//
//        if(PainterAPI.INSTANCE.remove(new BlockFace(pos, Direction.UP))) {
//            player.level().playLocalSound(pos, SoundEvents.AXE_SCRAPE, SoundSource.BLOCKS, 0.5f, 2.0f, false);
//            return true;
//        }
//
//        return false;
//
//    }

    public static void handle(BrushPacket packet, ServerAccess access) {

        ItemStack stack = access.player().getItemInHand(access.player().getUsedItemHand());

        CompoundTag tag = stack.getTag();
        if(tag == null) {
            tag = new CompoundTag();
            stack.setTag(tag);
        }
        tag.putInt("variant", packet.variant());
    }

    
}
