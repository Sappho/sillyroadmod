package uwu.serenya.silly_road.items.paint;

import company.sappho.painter.PainterAPI;
import company.sappho.painter.api.BlockFace;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

public class PaintScraperItem extends Item {
    public PaintScraperItem(Properties properties) {
        super(properties);
    }

    @Override
    public InteractionResult useOn(UseOnContext context) {
        BlockState state = context.getLevel().getBlockState(context.getClickedPos());

        if(!PainterAPI.INSTANCE.isPainted(context.getLevel(), new BlockFace(context.getClickedPos(), context.getClickedFace()))) return InteractionResult.PASS;

        PainterAPI.INSTANCE.remove(context.getLevel(),new BlockFace(context.getClickedPos(), context.getClickedFace()));
        context.getPlayer().level().playLocalSound(context.getClickedPos(), SoundEvents.AXE_SCRAPE, SoundSource.BLOCKS, 0.5f, 2.0f, false);
        return InteractionResult.SUCCESS;


    }
}
