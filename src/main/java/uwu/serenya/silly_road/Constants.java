package uwu.serenya.silly_road;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Constants {
    public static final String MOD_ID = "sillyroads";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);
    public static int LINE_COUNT = 323;
}
