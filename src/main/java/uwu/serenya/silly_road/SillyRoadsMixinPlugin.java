package uwu.serenya.silly_road;

import net.fabricmc.loader.api.FabricLoader;
import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import java.util.List;
import java.util.Set;

public class SillyRoadsMixinPlugin implements IMixinConfigPlugin {

    public static final List<String> AUTOMOBILITY_MIXINS = List.of("MixinSlopeBlock", "MixinSlopeModel", "MixinSlopeModelAccessor");


    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        if(AUTOMOBILITY_MIXINS.contains(mixinClassName)) return FabricLoader.getInstance().isModLoaded("automobility");
        return true;
    }

    @Override
    public void onLoad(String mixinPackage) {

    }

    @Override
    public String getRefMapperConfig() {
        return null;
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {

    }

    @Override
    public List<String> getMixins() {
        return null;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }
}
