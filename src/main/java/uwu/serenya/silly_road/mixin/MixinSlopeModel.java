package uwu.serenya.silly_road.mixin;

import company.sappho.painter.api.BlockFace;
import company.sappho.painter.api.PainterSprite;
import company.sappho.painter.client.PainterAPIClient;
import company.sappho.painter.data.PaintedObject;
import company.sappho.painter.data.SpriteLayer;
import io.github.foundationgames.automobility.block.SlopeBlock;
import io.github.foundationgames.automobility.block.model.GeometryBuilder;
import io.github.foundationgames.automobility.block.model.SlopeUnbakedModel;
import io.github.foundationgames.automobility.fabric.block.render.FabricGeometryBuilder;
import io.github.foundationgames.automobility.fabric.block.render.FabricSlopeBakedModel;
import net.fabricmc.fabric.api.renderer.v1.mesh.QuadEmitter;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.block.state.BlockState;
import org.joml.Vector3f;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uwu.serenya.silly_road.util.Util;

import java.util.function.Supplier;

@Mixin(value = FabricSlopeBakedModel.class, remap = false)
public abstract class MixinSlopeModel {

    @Inject(
            method = "emitBlockQuads",
            at = @At("TAIL")
    )
    public void drawLine(BlockAndTintGetter view, BlockState state, BlockPos pos, Supplier<RandomSource> randomSupplier, RenderContext context, CallbackInfo ci) {

        QuadEmitter emitter = context.getEmitter();
        MixinSlopeModelAccessor accessor = (MixinSlopeModelAccessor) this;
        BlockFace faceBelow = new BlockFace(pos.below(), Direction.UP);

        if(!PainterAPIClient.ACTIVE.isPainted(faceBelow)) return;

        PaintedObject object = PainterAPIClient.ACTIVE.getItem(faceBelow);
        PainterSprite sprite = object.getPaintedSprite();

        int color = object.color;

        Direction lineDir = Direction.fromYRot(object.rotation);
        //if(lineDir.getAxis() == Direction.Axis.X) lineDir = lineDir.getOpposite();

        lineDir = switch (state.getValue(SlopeBlock.FACING)) { // Rotate
            case NORTH -> lineDir;
            case SOUTH -> lineDir.getOpposite();
            case EAST -> lineDir.getClockWise();
            case WEST -> lineDir.getCounterClockWise();

            default -> throw new IllegalStateException();
        };

        int[][] uv = Util.getUVDirection(lineDir);

        SlopeUnbakedModel.Type type = accessor.getType();
        boolean steep = type == SlopeUnbakedModel.Type.STEEP;
        float height = steep ? 1 : 0.5f;
        float rise = steep ? 0 : (type == SlopeUnbakedModel.Type.TOP ? 0.5f : 0);

        GeometryBuilder builder = new FabricGeometryBuilder(emitter, accessor.getSettings().getRotation().getMatrix());
        Vector3f normal = new Vector3f(0, 1, -height);
        normal.normalize();

        Vector3f offset = new Vector3f(normal);
        offset.mul(0.001f);

        for(SpriteLayer layer : sprite.layers()) {
            TextureAtlasSprite texture = layer.sprite().get();
            offset.mul(1.01f);

            builder.vertex(1, rise + offset.y(), offset.z(), null, normal.x(), normal.y(), normal.z(), texture, uv[1][0], uv[1][1], color)
                    .vertex(0, rise + offset.y(), offset.z(), null, normal.x(), normal.y(), normal.z(), texture , uv[2][0], uv[2][1], color)
                    .vertex(0, rise + height + offset.y(), 1 + offset.z(), null, normal.x(), normal.y(), normal.z(), texture, uv[3][0], uv[3][1], color)
                    .vertex(1, rise + height + offset.y(), 1 + offset.z(), null, normal.x(), normal.y(), normal.z(), texture, uv[0][0], uv[0][1], color);
        }


    }
}
