package uwu.serenya.silly_road.mixin;


import company.sappho.painter.PainterAPI;
import company.sappho.painter.api.BlockFace;
import net.minecraft.client.color.block.BlockColors;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.MapColor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import uwu.serenya.silly_road.util.RoadColor;

import java.util.Optional;

@Mixin(BlockColors.class)
public class MixinBlockColors {

    @Inject(
            method = "getColor(Lnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/world/level/Level;Lnet/minecraft/core/BlockPos;)I",
            at = @At("HEAD"),
            cancellable = true
    )
    public void getPaintedColor(BlockState state, Level level, BlockPos pos, CallbackInfoReturnable<Integer> cir) {
        BlockFace topFace = new BlockFace(pos, Direction.UP);
        if(PainterAPI.INSTANCE.isPainted(level, topFace)) {
            Optional<RoadColor> roadColor = RoadColor.fromColor(PainterAPI.INSTANCE.ofLevel(level).getItem(topFace).color);
            roadColor.ifPresent(c -> cir.setReturnValue(c.color));
        }
    }
}
