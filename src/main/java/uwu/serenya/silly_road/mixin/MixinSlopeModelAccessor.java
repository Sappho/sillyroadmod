package uwu.serenya.silly_road.mixin;

import io.github.foundationgames.automobility.block.model.SlopeBakedModel;
import io.github.foundationgames.automobility.block.model.SlopeUnbakedModel;
import net.minecraft.client.resources.model.ModelState;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(value = SlopeBakedModel.class, remap = false)
public interface MixinSlopeModelAccessor {

    @Accessor("settings")
    ModelState getSettings();

    @Accessor("type")
    SlopeUnbakedModel.Type getType();
}
