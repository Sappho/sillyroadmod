package uwu.serenya.silly_road.network;

import net.minecraft.nbt.CompoundTag;

public record PainterInitPacket(CompoundTag nbt) {

}
