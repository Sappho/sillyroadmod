package uwu.serenya.silly_road.network;

import io.wispforest.owo.network.OwoNetChannel;
import uwu.serenya.silly_road.SillyRoads;
import uwu.serenya.silly_road.items.paint.PaintBrushItem;

public class SillyRoadsNetwork {
    public static final OwoNetChannel BRUSH = OwoNetChannel.create(SillyRoads.asLocation("brush"));

    public static void register() {
        BRUSH.registerServerbound(BrushPacket.class, PaintBrushItem::handle);
    }


}
